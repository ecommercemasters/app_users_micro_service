import { listByIdUserTypes, signup, login, updateUserType, listUserTypes, deleteUserType } from "../../repositories/users/usersRepository.js";
import UserType from "../../repositories/users/usersRepositoryModel.js";

export class UserService {
    signupService = async (userModelService)=> {
        let userRepo = new UserType(userModelService)
        let user = await signup(userRepo)
        return user
    }
    
    loginService = async (userModelService)=> {
        let userRepo = new UserType(userModelService)
        let user = await login(userRepo)
        
        if(userModelService.password == user.password)
            return user
        return null
    }
    
    updateUserService = async (userId, userModelService)=> {
        let userRepo = new UserType(userModelService)
        let user = await updateUserType(userId, userRepo)
        return user
    }
    
    listByIdUserService = async() => {
        let users = await listByIdUserTypes();
        return users
    }
    
    listUserService = async() => {
        let users = await listUserTypes();
        return users
    }
    
    deleteUserService = async (userId)=> {
        let user = await deleteUserType(userId)
        return user
    }
}