import mongoose from 'mongoose';

export class UserType {

    constructor(firstname, lastname, address, codePhone, phone, email, password, _id = undefined) {
        this._id = _id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.codePhone = codePhone;
        this.phone = phone;
        this.email = email;
        this.password = password;
        return this;
    }

    get() {
        return this;
    }

    //GETTERS
    getFirstname() {
        return this.firstname;
    }
    getLastname() {
        return this.lastname;
    }
    getAddress() {
        return this.address;
    }
    getCodePhone() {
        return this.codePhone;
    }
    getPhone() {
        return this.phone;
    }
    getEmail() {
        return this.email;
    }
    getPassword() {
        return this.password;
    }

    //SETTERS
    setFirstname(firstname) {
        this.firstname = firstname;
        return this;
    }
    setLastname(lastname) {
        this.lastname = lastname;
        return this;
    }
    setAdress(address) {
        this.address = address;
        return this;
    }
    setCodePhone(codePhone) {
        this.codePhone = codePhone;
        return this;
    }
    setPhone(phone) {
        this.phone = phone;
        return this;
    }
    setEmail(email) {
        this.email = email;
        return this;
    }
    setPassword(password) {
        this.password = password;
        return this;
    }
}

const userSchema = mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    address: { type: String, required: true },
    codePhone: { type: Number, required: true },
    phone: { type: Number, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true }
});

// export default userSchema
export default mongoose.model('User', userSchema);