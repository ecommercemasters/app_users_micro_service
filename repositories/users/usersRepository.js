import userSchema from "./usersRepositoryModel.js";
import { UserType } from "./usersRepositoryModel.js";

const doc = new userSchema()

const signup = async (userObject) => {
    // doc = userObject
    doc.firstname = userObject.firstname
    doc.lastname = userObject.lastname
    doc.address = userObject.address
    doc.codePhone = userObject.codePhone
    doc.phone = userObject.phone
    doc.email = userObject.email
    doc.password = userObject.password
    console.log(doc)
    return await doc.save()
    .catch((error) => { console.log(error); return null; });
}

const login = async (userObject) => {
    return await userSchema.findOne({ email: userObject.email })
    .catch((error) => { console.log(error); return null });
}

const updateUserType = async ( userId, user) => {
    return await userSchema.updateOne({ _id: userId }, { ...user, _id: userId })
    .catch((error) => { return null; });
}

const listUserTypes = async () => {
    return await userSchema.find()
    .catch((error) => { console.log(error); return null });
}

const listByIdUserTypes = async (userId) => {
    return await userSchema.findOne(userId)
    .catch((error) => { console.log(error); return null });
}

const deleteUserType = async (userId) => {
    return await userSchema.deleteOne({ _id: userId })
    .catch((error) => { console.log(error); return false; });
}

export { signup, login, updateUserType, listUserTypes, listByIdUserTypes, deleteUserType }