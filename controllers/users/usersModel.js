export default class UserModel {

    constructor(firstname, lastname, address, codePhone, phone, email, password, _id = undefined) {
        this._id = _id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.codePhone = codePhone;
        this.phone = phone;
        this.email = email;
        this.password = password;
        return this;
    }

    get() {
        return this;
    }

    //GETTERS
    getFirstname() {
        return this.firstname;
    }
    getLastname() {
        return this.lastname;
    }
    getAddress() {
        return this.address;
    }
    getCodePhone() {
        return this.codePhone;
    }
    getPhone() {
        return this.phone;
    }
    getEmail() {
        return this.email;
    }
    getPassword() {
        return this.password;
    }

    //SETTERS
    setFirstname(firstname) {
        this.firstname = firstname;
        return this;
    }
    setLastname(lastname) {
        this.lastname = lastname;
        return this;
    }
    setAdress(address) {
        this.address = address;
        return this;
    }
    setCodePhone(codePhone) {
        this.codePhone = codePhone;
        return this;
    }
    setPhone(phone) {
        this.phone = phone;
        return this;
    }
    setEmail(email) {
        this.email = email;
        return this;
    }
    setPassword(password) {
        this.password = password;
        return this;
    }
}