import {UserService} from "../../services/users/usersService.js";

import UserModel from "./usersModel.js";
import express from 'express';
import UserModelService from "../../services/users/usersServiceModel.js";

const router = express.Router();

router.get('/', async (req, res, next) => {
    let users = await new UserService().listUserService()
    res.status(200).send(users);
});

router.get('/:id', async (req, res, next) => {
    let users = await new UserService().listByIdUserService(req.params.id)
    res.status(200).send(users);
});

router.post('/signup', async (req, res, next) => {
    let user = new UserModel(req.body.firstname, req.body.lastname, req.body.address, req.body.codePhone, req.body.phone, req.body.email, req.body.password)
    let userCreate = await new UserService().signupService(user)
    res.status(201).send(userCreate)
});

router.put('/', async (req, res, next) => {
    let user = new UserModel(req.body.firstname, req.body.lastname, req.body.address, req.body.codePhone, req.body.phone, req.body.email, req.body.password)
    let userUpdated = await new UserService().updateUserService(user)
    res.status(201).send(userUpdated)
});

router.delete('/:id', async (req, res, next) => {
    let userDeleted = await new UserService().deleteUserService(req.params.id)
    res.status(201).send(userDeleted)
});

router.post('/login', async (req, res, next) => {
    let user = new UserModel("", "", "", "", "", req.body.email, req.body.password)
    let userLog = await new UserService().loginService(user)
    res.status(200).send(userLog)
});

export default router;