import express from 'express';
import mongoose from 'mongoose';
import router from './controllers/users/usersController.js';
import cors from 'cors'

mongoose.connect('mongodb+srv://admin:MVM75zX81NZJNJmf@clusterfree.onhaoec.mongodb.net/app_users',
{ useNewUrlParser: true,
    useUnifiedTopology: true }
  )
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));
  
const app = express();

app.use(cors())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use(express.json());

app.use('/api/user', router);

export default app;